package szerjavic.mybatis.demo.xml.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;
import szerjavic.mybatis.demo.model.Person;

import java.util.List;
import java.util.Optional;

@Mapper
@Repository
public interface PersonXmlMapper {

    List<Person> findAllPersons();

    Optional<Person> findPersonById(Integer id);

    Integer insertPerson(Person person);

}
