package szerjavic.mybatis.demo.java.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import szerjavic.mybatis.demo.model.Person;

import java.util.List;
import java.util.Optional;

@Mapper
public interface PersonJavaMapper {

    @Select("SELECT * FROM person")
    List<Person> findAllPersons();

    @Select("SELECT * FROM person WHERE id=#{id}")
    Optional<Person> findPersonById(Integer id);


    @Insert(value = "INSERT INTO person (name) VALUES(#{name})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    Integer insertPerson(Person person);
}
