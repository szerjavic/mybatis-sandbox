package szerjavic.mybatis.demo.java.mapper;

import config.JavaTestConfig;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import szerjavic.mybatis.demo.model.Person;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ContextConfiguration(classes = JavaTestConfig.class)
@ExtendWith({SpringExtension.class})
class PersonJavaMapperTest {

    @Autowired
    private PersonJavaMapper personJavaMapper;

    @Test
    void findAllPersons() {
    }

    @Test
    void findPersonById() {

        Person person = new Person();
        person.setName("name");

        Integer primaryKey = personJavaMapper.insertPerson(person);

        Optional<Person> optional = personJavaMapper.findPersonById(primaryKey);

        assertEquals(true, optional.isPresent());
        assertEquals("name", optional.get().getName());
    }

    @Test
    void insertPerson() {

        Person person = new Person();
        person.setName("name");

        personJavaMapper.insertPerson(person);

    }
}