package szerjavic.mybatis.demo.xml.mapper;

import config.XmlTestConfig;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import szerjavic.mybatis.demo.model.Person;

import java.util.List;
import java.util.Optional;

@ContextConfiguration(classes = XmlTestConfig.class)
@ExtendWith({SpringExtension.class})
class PersonXmlMapperTest {

    @Autowired
    private PersonXmlMapper personXmlMapper;

    @Test
    public void testFindPersonById() {
        Person person = new Person();
        person.setName("name");

        Integer primaryKey = personXmlMapper.insertPerson(person);
        Optional<Person> optional = personXmlMapper.findPersonById(primaryKey);

        Assertions.assertEquals("name", optional.get().getName());

    }

    @Test
    public void testFindAllPersons() {
        Person person = new Person();
        person.setName("name");
        personXmlMapper.insertPerson(person);
        personXmlMapper.insertPerson(person);
        personXmlMapper.insertPerson(person);

        List<Person> persons = personXmlMapper.findAllPersons();

        Assertions.assertEquals(3, persons.size());


    }

}